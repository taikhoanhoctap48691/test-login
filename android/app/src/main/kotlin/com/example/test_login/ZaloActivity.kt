package com.example.test_login

import android.content.Intent
import android.text.TextUtils
import android.util.Base64
import com.zing.zalo.zalosdk.oauth.*
import com.zing.zalo.zalosdk.oauth.model.ErrorResponse
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import java.security.MessageDigest
import java.security.SecureRandom
import org.json.JSONObject


class ZaloActivity : FlutterActivity() {

    private val channelName = "com.example.test_login/zalo"

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger,
            channelName
        ).setMethodCallHandler { call, result ->
            try {
                when (call.method) {
                    "login" -> {
                        loginZalo(result)
                    }
                    "logout" -> {
                        logOut(result)
                    }
                    "getProfile" -> {
                        getProfile(result)
                    }
                    else -> {
                        result.notImplemented()
                    }
                }
            } catch (e: Exception) {
                result.success(e.toString())
            }
        }
    }

    private fun getProfile(result: MethodChannel.Result) {
        val fields = arrayOf("id", "birthday", "gender", "picture", "name")
        val accessToken = ""

        ZaloSDK.Instance.getAccessTokenByOAuthCode(
            this, accessToken, "",
            ZaloOpenAPICallback { jsonObject: JSONObject? ->
            },
        )

        ZaloSDK.Instance.getProfile(
            this, accessToken,
            ZaloOpenAPICallback { jsonObject: JSONObject? ->
                if (jsonObject == null) {
                    result.success(null)
                } else {
                    result.success(ZaloSDK.Instance.zaloDisplayname)
                }
            },
            fields,
        )
    }

    private fun logOut(result: MethodChannel.Result) {
        ZaloSDK.Instance.unauthenticate()
        result.success(null)
    }

    private fun loginZalo(result: MethodChannel.Result) {
        val verifyCode = generateCodeVerifier()
        val challengeCode = generateCodeChallenge(verifyCode)

        val listener = object : OAuthCompleteListener() {
            override fun onGetOAuthComplete(response: OauthResponse?) {
                if (TextUtils.isEmpty(response?.oauthCode)) {
                    println("❌❌❌❌❌❌❌❌❌❌❌ ERROR: ${response?.errorCode} ${response?.errorMessage}")
                } else {
                    println("✅✅✅✅✅✅✅✅✅✅✅ LOGIN SUCCESS")

                    val map: MutableMap<String, Any?> = HashMap()
                    map["isSuccess"] = true
                    map["oauthCode"] = response?.oauthCode
                    map["refreshToken"] = response?.refreshToken
                    result.success(map)
                }
            }

            override fun onAuthenError(p0: ErrorResponse?) {
                println("❌❌❌❌❌❌❌❌❌❌❌ ERROR: ${p0?.errorCode} ${p0?.errorMsg}")

                val map: MutableMap<String, Any?> = HashMap()
                map["isSuccess"] = false
                map["errorCode"] = p0?.errorCode
                map["errorMsg"] = p0?.errorMsg
                result.success(map)
            }
        }

        ZaloSDK.Instance.authenticateZaloWithAuthenType(
            this,
            LoginVia.APP_OR_WEB,
            challengeCode,
            listener
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data);
        ZaloSDK.Instance.onActivityResult(this, requestCode, resultCode, data);
    }

    private fun trimSpecialChars(str: String): String {
        return str.trim().replace("+", "-")
            .replace("/", "_").replace("=", "").replace("\\n", "")
    }

    private fun generateCodeVerifier(): String? {
        return try {
            val secureRandom = SecureRandom()
            val codeVerifier = ByteArray(32)
            secureRandom.nextBytes(codeVerifier)
            val encoded = Base64.encodeToString(codeVerifier, Base64.DEFAULT)
            trimSpecialChars(encoded)
        } catch (e: Exception) {
            null
        }
    }

    private fun generateCodeChallenge(verifyCode: String?): String? {
        return try {
            val bytes = verifyCode?.toByteArray(charset("US-ASCII"))
            val messageDigest = MessageDigest.getInstance("SHA-256")
            messageDigest.update(bytes!!, 0, bytes.size)
            val digest = messageDigest.digest()
            val encoded = Base64.encodeToString(digest, Base64.DEFAULT)
            trimSpecialChars(encoded)
        } catch (e: Exception) {
            null
        }
    }
}
