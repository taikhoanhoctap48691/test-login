import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:test_login/home_page.dart';

class AuthPage extends StatelessWidget {
  const AuthPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FilledButton(
          onPressed: () {
            _authWithZalo(() {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => const HomePage(),
              ));
            });
          },
          child: const Text('Login with Zalo'),
        ),
      ),
    );
  }

  FutureOr<void> _authWithZalo(VoidCallback onCallback) async {
    try {
      const channel = MethodChannel("com.example.test_login/zalo");

      final Map<dynamic, dynamic>? data = await channel.invokeMethod('login');

      final logger = Logger();

      if (data != null && data["isSuccess"]) {
        logger.i(data.toString());
        onCallback.call();
      }
    } catch (e) {
      log(e.toString());
    }
  }
}
